import iCal from 'ical.js';
import moment from 'moment';

import calendar from './calendar.ics';

fetch(`${calendar}`)
    .then(res => res.text())
    .then(text => iCal.parse(text))
    .then(jcal => render(jcal));

const view = document.getElementById('calendar');

document.getElementById('today').innerHTML = moment().format("dddd, MMMM Do YYYY");

setTimeout(() => window.location.reload(), 60000);

function render(jcalData) {
    const json = makeJSONish(jcalData);
    const events = json.events;
    const filtered = json.events.filter(event => {
        const endDate = moment(event.endDate.toJSDate());
        const startDate = moment(event.startDate.toJSDate());
        return moment().isBetween(startDate, endDate);
    })
    if (filtered.length < 1) {
        const msg = document.createElement('div');
        msg.classList.add('message');
        msg.innerText = 'No Events Today!';
        view.appendChild(msg);
        return;
    }
    const table = document.createElement('table');
    filtered.forEach(event => {
        const rowCount = table.rows.length;
        const row = table.insertRow(rowCount);
        const cell1 = row.insertCell(0);
        cell1.classList.add('time');
        cell1.innerHTML = 
            `<td>${moment(event.startDate.toJSDate()).format('h:mm a')} - ${moment(event.endDate.toJSDate()).format('h:mm a')}</td>`;
        const cell2 = row.insertCell(1);
        cell2.innerHTML = 
            `<td>
            <p><strong>${event.summary || ''} ${event.location ? `@ ${event.location}` : ''}</strong></p>
            <p>${event.description ? event.description.slice(0, 50) : 'No Description'}</p>
            </td>`
    })
    view.appendChild(table);
}

function makeJSONish(jcalData) {
    const comp = new iCal.Component(jcalData);
    const events = comp.getAllSubcomponents('vevent').map(event => {
        return new iCal.Event(event);
    });
    return { events }
}