const path = require('path');
const fs = require('fs');

const distDir = path.join(process.cwd(), 'dist');
const calendarPath = path.join(distDir, 'calendar.ics');

fs.readdir(distDir, (err, files) => {
  if (err) {
    console.log('Error: ', err);
    process.exit();
  } else {
    const icsFile = files.find((file) => {
      return file.slice(-3) === 'ics';
    })
    const moduleName = icsFile.slice(0, -4);
    fs.renameSync(path.join(distDir, icsFile), calendarPath);
    const fibrous = path.join(distDir, 'fibrous.js');
    console.log(moduleName);
    fs.readFile(fibrous, 'utf8', (err, data) => {
      const regex = new RegExp(moduleName, 'g');
      const newData = data.replace(regex, 'calendar');
      fs.writeFileSync(fibrous, newData);
    })
  }
})