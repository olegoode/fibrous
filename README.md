# fibrous

fibrous is quite handy for displaying today's schedule (from an .ics file) in the browser.

Designed for a Raspberry Pi.

## Development

Clone this repo, then install dependencies with `npm install`.

Ensure `calendar.ics` is located in the project's root folder before attempting to run or build.

Run `npm start` to run Parcel's development server.

## Usage

Run `npm run build` for a minified version. Then open `dist/index.html` in your browser of choice.